#!/usr/bin/env bash

data_path=$HOME/VisualSearch/
run_id=0
config_name=efficientnet-b7-ns_lr5e-5decay0.8_finetune.json
train_collection=xxx
val_collection=xxx

GPU=$1
prefix=$2
python3 train_landmarks.py \
--data_path $data_path \
--train_collection $train_collection \
--val_collection $val_collection \
--run_id $run_id \
--config_name $config_name \
--gpu $GPU \
--resume none \
--seed 777 \
--prefix $prefix \
--workers 6 \
